import argparse
import os

import numpy as np

import cv2
from mark_detector import MaskDetectorDLIB
from pose_estimator import PoseEstimator
from stabilizer import Stabilizer

CNN_INPUT_SIZE = 128


def get_face(detector, img_queue, box_queue):
    """Get face from image queue. This function is used for multiprocessing"""
    while True:
        image = img_queue.get()
        box = detector.extract_cnn_facebox(image)
        if box is not None:
            print("Face box detected yey!")
            box_queue.put(box)
        else:
            print("Face box not found")


def start_camera(mark_detector, pose_stabilizers):
    # Video source from webcam or video file.
    video_src = 0
    cam = cv2.VideoCapture(video_src)
    _, sample_frame = cam.read()

    height, width = sample_frame.shape[:2]
    pose_estimator = PoseEstimator(img_size=(height, width))

    while True:
        # Read frame, crop it, flip it, suits your needs.
        frame_got, frame = cam.read()
        if frame_got is False:
            break

        # If frame comes from webcam, flip it so it looks like a mirror.
        if video_src == 0:
            frame = cv2.flip(frame, 2)

        # Feed frame to image queue.
        facebox = mark_detector.extract_cnn_facebox(frame)

        pose, euler = pose_estimator.get_pose(frame, facebox, mark_detector, pose_stabilizers)
        frame = pose_estimator.draw_annotation_box(frame, pose[0], pose[1], color=(128, 255, 128))

        # Show preview.
        cv2.imshow("Preview", frame)
        if cv2.waitKey(10) == 27:
            break


def main(camera, images_path, output_path, show):
    """MAIN"""
    # Introduce mark_detector to detect landmarks.
    mark_detector = MaskDetectorDLIB()

    if camera:
        # Introduce scalar stabilizers for pose.
        pose_stabilizers = [Stabilizer(
            state_num=2,
            measure_num=1,
            cov_process=0.1,
            cov_measure=0.1) for _ in range(6)]
        start_camera(mark_detector, pose_stabilizers)
        return # In camera mode it did not store results

    for file_path in os.listdir(images_path):
        if not file_path.lower().endswith('.png') and not file_path.lower().endswith('.jpg'):
            continue # skip the file

        print("Processing file: '%s'" % file_path)
        image_name = os.path.splitext(file_path)[0]

        image = cv2.imread(os.path.join(images_path, file_path))
        # Introduce pose estimator to solve pose. Get one frame to setup the
        # estimator according to the image size.
        height, width = image.shape[:2]
        pose_estimator = PoseEstimator(img_size=(height, width))

        # Get face from box queue.
        facebox = mark_detector.extract_cnn_facebox(image)

        # Introduce scalar stabilizers for pose.
        pose_stabilizers = [Stabilizer(
            state_num=2,
            measure_num=1,
            cov_process=0.1,
            cov_measure=0.1) for _ in range(6)]

        # Stabilize pose calc
        for _ in range(10):
            pose, euler = pose_estimator.get_pose(image, facebox, mark_detector, pose_stabilizers)

        if show:
            image = pose_estimator.draw_annotation_box(image, pose[0], pose[1], color=(128, 255, 128))
            while True:
                # Show preview.
                cv2.imshow("Preview", image)
                if cv2.waitKey(10) == 27:
                    break

        if output_path is None:
            output_path = images_path

        np.savetxt(os.path.join(output_path, image_name + ".pose"), euler)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    arg = parser.add_argument
    arg('--camera', type=bool, default=False, help="Take input image from camera if set to True ignores --image_path")
    arg('--images_path', type=str, default="", help="Path to the images to process")
    arg('--show', type=bool, default=False, help="Show result on the screen")
    arg('--output_path', type=str, default=None, help="Result text files output path file name format {image_file_name}.pose")
    args = parser.parse_args()
    main(args.camera, args.images_path, args.output_path, args.show)
