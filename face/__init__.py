from collections import OrderedDict
from face.detected_face import DetectedFace

FACIAL_LANDMARKS_IDXS = OrderedDict([
        ("mouth", (48, 68)),
        ("right_eyebrow", (17, 22)),
        ("left_eyebrow", (22, 27)),
        ("right_eye", (36, 42)),
        ("left_eye", (42, 48)),
        ("nose", (27, 35)),
        ("jaw", (0, 17))
    ])


def get_faces(image, detector, predictor):
    for face in detector(image, 0):
        landmarks = predictor(image, face)

        yield DetectedFace(image[face.top(): face.bottom(), face.left(): face.right()],
                           face.left(), face.right() - face.left(), face.top(), face.bottom() - face.top(), landmarks)
