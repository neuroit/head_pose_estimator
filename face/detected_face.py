import face
import numpy as np

class DetectedFace(object):
    def __init__(self, image, x, w, y, h, landmarks):
        self.image = image
        self.x = x
        self.w = w
        self.y = y
        self.h = h
        self.landmarks = landmarks

    def landmarksAsXY(self):
        return [(p.x, p.y) for p in self.landmarks.parts()]

    def landmarksAsVector(self):
        vec = np.empty([68, 2], dtype=int)
        for b in range(68):
            vec[b][0] = self.landmarks.part(b).x
            vec[b][1] = self.landmarks.part(b).y

        return vec

    def get_landmarks_by_name(self, name):
        face_landmarks = self.landmarks.landmarksAsXY()
        (j, k) = face.FACIAL_LANDMARKS_IDXS[name]
        return face_landmarks[j:k]

    def get_base_landmarks_as_vector(self):
        vec = np.empty([6, 2], dtype=int)
        # nose tip
        vec[0][0] = self.landmarks.part(30).x
        vec[0][1] = self.landmarks.part(30).y

        # chin tip
        vec[1][0] = self.landmarks.part(8).x
        vec[1][1] = self.landmarks.part(8).y

        # left eye outer corner
        vec[2][0] = self.landmarks.part(45).x
        vec[2][1] = self.landmarks.part(45).y

        # right eye outer corner
        vec[3][0] = self.landmarks.part(36).x
        vec[3][1] = self.landmarks.part(36).y

        # mouth left corner
        vec[4][0] = self.landmarks.part(54).x
        vec[4][1] = self.landmarks.part(54).y

        # mouth right corner
        vec[5][0] = self.landmarks.part(48).x
        vec[5][1] = self.landmarks.part(48).y

        return vec
